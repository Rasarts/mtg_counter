# MtgCounter  [![Actions Status](https://github.com/Hecatoncheir/mtg_counter/workflows/check/badge.svg)](https://github.com/Hecatoncheir/mtg_counter/actions)

[![pipeline status](https://gitlab.com/Rasarts/mtg_counter/badges/master/pipeline.svg)](https://gitlab.com/Rasarts/mtg_counter/commits/master) [![coverage report](https://gitlab.com/Rasarts/mtg_counter/badges/master/coverage.svg)](https://gitlab.com/Rasarts/mtg_counter/commits/master)

Magic the gathering life counter application.

## mtg_counter

preview


## Unit tests
`flutter test --coverage`

`flutter test --platform chrome --coverage`

## Integration tests

`flutter drive --target=test_driver/splash/application.dart`
