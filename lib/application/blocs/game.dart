import 'dart:async';
import 'dart:convert';

import 'package:meta/meta.dart' show required;

import 'package:mtg_counter/application/blocs/player.dart' as blocs;

import 'package:mtg_counter/application/models/game.dart' as models;
import 'package:mtg_counter/application/models/player.dart' as models;

import 'package:mtg_counter/application/resources/read_writer/read_writer.dart'
    as resources;

enum GameEventType { gameLoaded, playerAdded, playerRemoved }

class GameEvent {
  GameEventType gameEventType;
  List<blocs.Player> players;

  blocs.Player changedPlayer;

  GameEvent({this.gameEventType, this.players, this.changedPlayer});
}

class Game {
  final models.Game gameModel;
  final resources.ReadWriter readWriter;

  List<blocs.Player> players = [];

  final StreamController<GameEvent> gameController =
      StreamController<GameEvent>();
  Stream<GameEvent> game;

  Game({@required this.gameModel, @required this.readWriter}) {
    if (gameModel.players.isNotEmpty) {
      for (models.Player playerModel in gameModel.players) {
        final playerBloc = blocs.Player(playerModel);
        players.add(playerBloc);

        playerBloc.playerEvents
            .listen((_) => readWriter.write(json.encode(gameModel.toMap())));
      }
    }

    readWriter.write(json.encode(gameModel.toMap()));

    game = gameController.stream.asBroadcastStream();
  }

  void dispose() {
    gameController.close();
  }

  void load() => Future.delayed(
      Duration(milliseconds: 400),
      () => gameController.add(GameEvent(
          gameEventType: GameEventType.gameLoaded, players: players)));

  void addPlayer(models.Player playerModel) {
    playerModel.lifeCount = gameModel.startLifeCount;
    gameModel.players.add(playerModel);

    final playerBloc = blocs.Player(playerModel);
    players.add(playerBloc);

    playerBloc.playerEvents
        .listen((_) => readWriter.write(json.encode(gameModel.toMap())));

    readWriter?.write(json.encode(gameModel.toMap()));

    gameController.add(GameEvent(
        gameEventType: GameEventType.playerAdded,
        players: players,
        changedPlayer: playerBloc));
  }

  void removePlayer(models.Player playerModel) {
    gameModel.players.remove(playerModel);

    blocs.Player removedPlayerBloc;
    for (blocs.Player playerBloc in players) {
      if (playerBloc.playerModel.id != playerModel.id) continue;
      removedPlayerBloc = playerBloc;
      players.remove(playerBloc);
      break;
    }

    readWriter?.write(json.encode(gameModel.toMap()));

    gameController.add(GameEvent(
        gameEventType: GameEventType.playerRemoved,
        players: players,
        changedPlayer: removedPlayerBloc));
  }
}
