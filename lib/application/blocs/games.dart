import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:meta/meta.dart' show required;

import 'package:mtg_counter/application/models/game.dart' as models;
import 'package:mtg_counter/application/models/game_details.dart' as models;
import 'package:mtg_counter/application/models/games.dart' as models;
import 'package:mtg_counter/application/models/player.dart' as models;

import 'package:mtg_counter/application/blocs/game.dart' as blocs;

import 'package:mtg_counter/application/resources/read_writer/read_writer.dart'
    as resources;
import 'package:mtg_counter/application/resources/read_writer/file_read_writer.dart'
    as resources;

class Games {
  models.Games model;
  resources.ReadWriter readWriter;

  StreamController<Object> dependenciesController;

  Games(
      {@required this.model,
      @required this.readWriter,
      this.dependenciesController}) {
    readWriter.write(json.encode(model.toList()));
  }

  Future<models.Game> createGame() async {
    for (models.GameDetails gameDetails in model.details) {
      gameDetails.selected = false;
    }

    final game = models.Game();
    final filePath = '${game.id}.json';
    final gameFile = File(filePath)..createSync();
    final gameReadWriter = resources.FileReadWriter(file: gameFile);
    final gameBloc = blocs.Game(gameModel: game, readWriter: gameReadWriter);

    model.games.add(game);

    final gameDetails = models.GameDetails(
        id: game.id,
        dateTime: DateTime.now(),
        filePath: filePath,
        selected: true);

    model.details.add(gameDetails);

    await readWriter.write(json.encode(model.toList()));

    dependenciesController?.add(gameBloc);

    return game;
  }

  Future<models.Game> createGameFromAlreadyExist(
      models.Game alreadyExistGame) async {
    for (models.GameDetails gameDetails in model.details) {
      gameDetails.selected = false;
    }

    final players = <models.Player>[];

    for (models.Player player in alreadyExistGame.players) {
      player.lifeCount = alreadyExistGame.startLifeCount;
      players.add(player);
    }

    final game = models.Game(
        players: players, startLifeCount: alreadyExistGame.startLifeCount);

    final filePath = '${game.id}.json';

    final gameDetails = models.GameDetails(
        id: game.id,
        dateTime: DateTime.now(),
        filePath: filePath,
        selected: true);

    model.games.add(game);
    model.details.add(gameDetails);

    final gameFile = File(filePath)..createSync();
    final gameBloc = blocs.Game(
        gameModel: game, readWriter: resources.FileReadWriter(file: gameFile));

    await readWriter.write(json.encode(model.toList()));
    dependenciesController?.add(gameBloc);

    return game;
  }

  Future<void> selectGameById(String id) async {
    File gameFile;

    for (models.GameDetails gameDetails in model.details) {
      gameDetails.selected = false;
      if (gameDetails.id == id) {
        gameFile = File(gameDetails.filePath);
        gameDetails.selected = true;
      }
    }

    for (models.Game game in model.games) {
      if (game.id != id) continue;

      final gameBloc = blocs.Game(
          gameModel: game,
          readWriter: resources.FileReadWriter(file: gameFile));
      dependenciesController?.add(gameBloc);

      break;
    }
  }

  Future<void> deleteGame(models.Game game) async {
    final gameDetails = model.details.firstWhere((g) => g.id == game.id);

    model.games.remove(game);
    model.details.remove(gameDetails);

    File(gameDetails.filePath).deleteSync();

    if (model.games.isNotEmpty) {
      if (gameDetails.selected) {
        await selectGameById(model.details.last.id);
      }
    } else {
//      return createGame().then((game) => selectGameById(game.id));
    }

    await readWriter.write(json.encode(model.toList()));
  }
}
