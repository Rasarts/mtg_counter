import 'dart:async';

import 'package:mtg_counter/application/models/player.dart' as models;

enum PlayerEventType {
  nameChange,
  lifeCountChange,
  poisonCountChange,
  colorsChange
}

class PlayerEvent {
  PlayerEventType playerEventType;
  models.Player playerModel;

  PlayerEvent(this.playerEventType, this.playerModel);
}

class Player {
  final models.Player playerModel;

  final StreamController<PlayerEvent> playerEventsController =
      StreamController<PlayerEvent>();
  Stream<PlayerEvent> playerEvents;

  final StreamController<int> lifeCountController = StreamController<int>();
  Stream<int> lifeCount;

  final StreamController<int> poisonCountController = StreamController<int>();
  Stream<int> poisonCount;

  final StreamController<List<models.PlayerColor>> colorsController =
      StreamController<List<models.PlayerColor>>();
  Stream<List<models.PlayerColor>> colors;

  final StreamController<String> nameController = StreamController<String>();
  Stream<String> name;

  Player(this.playerModel) {
    playerEvents = playerEventsController.stream.asBroadcastStream();
    lifeCount = lifeCountController.stream.asBroadcastStream();
    poisonCount = poisonCountController.stream.asBroadcastStream();
    colors = colorsController.stream.asBroadcastStream();
    name = nameController.stream.asBroadcastStream();
  }

  void dispose() {
    playerEventsController.close();
    lifeCountController.close();
    poisonCountController.close();
    colorsController.close();
    nameController.close();
  }

  // ignore: use_setters_to_change_properties
  void changeName(String name) {
    playerModel.name = name;
    nameController.add(playerModel.name);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.nameChange, playerModel));
  }

  void addColor(models.PlayerColor playerColor) {
    playerModel.colors.add(playerColor);
    colorsController.add(playerModel.colors);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.colorsChange, playerModel));
  }

  void removeColor(models.PlayerColor playerColor) {
    playerModel.colors.remove(playerColor);
    colorsController.add(playerModel.colors);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.colorsChange, playerModel));
  }

  void addLifeQuantity(int count) {
    final updatedLifeCount = playerModel.lifeCount + count;
    playerModel.lifeCount = updatedLifeCount;
    lifeCountController.add(playerModel.lifeCount);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.lifeCountChange, playerModel));
  }

  void removeLifeQuantity(int count) {
    final updatedLifeCount =
        playerModel.lifeCount - (count > 0 ? count : count * -1);
    playerModel.lifeCount = updatedLifeCount;
    lifeCountController.add(playerModel.lifeCount);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.lifeCountChange, playerModel));
  }

  void addPoisonQuantity(int count) {
    final updatedPoisonCount = playerModel.poisonCount + count;
    playerModel.poisonCount = updatedPoisonCount;
    poisonCountController.add(playerModel.poisonCount);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.poisonCountChange, playerModel));
  }

  void removePoisonQuantity(int count) {
    final updatedPoisonCount =
        playerModel.poisonCount - (count > 0 ? count : count * -1);
    playerModel.poisonCount = updatedPoisonCount;
    poisonCountController.add(playerModel.poisonCount);

    playerEventsController
        .add(PlayerEvent(PlayerEventType.poisonCountChange, playerModel));
  }
}
