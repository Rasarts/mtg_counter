import 'package:uuid/uuid.dart';

import 'package:mtg_counter/application/models/player.dart' as models;

class Game {
  String id;
  DateTime dateTime;

  List<models.Player> players;

  int startLifeCount;

  Game({this.players, this.startLifeCount = 20}) {
    id = Uuid().v4();
    dateTime = DateTime.now();
    players = players != null ? players : [];
  }

  Game.fromMap(Map<String, dynamic> map) {
    if (map['id'] != null) id = map['id'];
    dateTime = DateTime.parse(map['dateTime']);
    players = (map['players'] as List)
        .map((player) => models.Player.fromMap(player))
        .toList();
    startLifeCount = map['startLifeCount'];
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'id': id,
        'dateTime': dateTime.toIso8601String(),
        'startLifeCount': startLifeCount,
        'players': players.map((player) => player.toMap()).toList(),
      };
}
