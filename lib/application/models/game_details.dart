class GameDetails {
  String id;
  DateTime dateTime;
  String filePath;
  bool selected;

  GameDetails({this.id, this.dateTime, this.filePath, this.selected = false});

  GameDetails.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    dateTime = map['dateTime'] != null ? DateTime.parse(map['dateTime']) : null;
    filePath = map['filePath'];
    selected = map['selected'];
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'id': id,
        'dateTime': dateTime?.toIso8601String(),
        'filePath': filePath,
        'selected': selected
      };
}
