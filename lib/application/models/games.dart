import 'package:mtg_counter/application/models/game.dart' as models;
import 'package:mtg_counter/application/models/game_details.dart' as models;

class Games {
  List<models.GameDetails> details;
  List<models.Game> games;

  Games({this.details, this.games}) {
    details = details != null ? details : [];
    games = games != null ? games : [];
  }

  Games.fromList(List<Map<String, dynamic>> list) {
    details = details != null ? details : [];
    games = games != null ? games : [];

    for (Map<String, dynamic> gameDetails in list) {
      details.add(models.GameDetails.fromMap(gameDetails));
    }
  }

  List<Map<String, dynamic>> toList() =>
      details.map((gameDetails) => gameDetails.toMap()).toList();
}
