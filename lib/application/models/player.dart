import 'package:meta/meta.dart' show required;
import 'package:uuid/uuid.dart' show Uuid;

enum PlayerColor { red, green, blue, white, black }

class Player {
  String id;
  String name;

  List<PlayerColor> colors;

  int lifeCount;
  int poisonCount;

  Player(
      {@required this.name,
      this.colors,
      this.lifeCount = 20,
      this.poisonCount = 0}) {
    colors ??= [];
    id = Uuid().v4();
  }

  Player.fromMap(Map<String, dynamic> map) {
    if (map['id'] != null) id = map['id'];
    name = map['name'];
    colors = decodePlayerColors(map['colors']);
    lifeCount = map['lifeCount'];
    poisonCount = map['poisonCount'];
  }

  List<PlayerColor> decodePlayerColors(List encodedColors) {
    final colors = <PlayerColor>[];
    if (encodedColors != null && encodedColors.isNotEmpty) {
      for (String encodedColor in encodedColors) {
        switch (encodedColor) {
          case 'red':
            colors.add(PlayerColor.red);
            break;
          case 'green':
            colors.add(PlayerColor.green);
            break;
          case 'blue':
            colors.add(PlayerColor.blue);
            break;
          case 'white':
            colors.add(PlayerColor.white);
            break;
          case 'black':
            colors.add(PlayerColor.black);
            break;
        }
      }
    }

    return colors;
  }

  Map<String, dynamic> toMap() => <String, dynamic>{
        'id': id,
        'name': name,
        'colors': encodePlayerColors(colors),
        'lifeCount': lifeCount,
        'poisonCount': poisonCount
      };

  List<String> encodePlayerColors(List<PlayerColor> decodedColors) {
    final colors = <String>[];

    if (decodedColors != null && decodedColors.isNotEmpty) {
      for (PlayerColor decodedColor in decodedColors) {
        colors.add(decodedColor.toString().split('.').last);
      }
    }

    return colors;
  }
}
