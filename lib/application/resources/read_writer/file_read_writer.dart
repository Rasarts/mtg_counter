import 'dart:io';

import 'read_writer.dart';

class FileReadWriter implements ReadWriter {
  @override
  String cache;
  File file;

  FileReadWriter({this.cache = '', this.file}) {
    if (cache.isEmpty && file != null) cache = file.readAsStringSync();
  }

  @override
  Future<void> write(String value) async {
    cache = value;
    file?.writeAsStringSync(value);
  }

  @override
  Future<String> read() async => cache;
}
