import 'package:universal_html/html.dart';
import 'read_writer.dart';

class LocalStorageReadWriter implements ReadWriter {
  @override
  String cache;
  String storage;

  LocalStorageReadWriter({this.cache = '', this.storage}) {
    if (storage == null) {
      throw ArgumentError.notNull("Storage name can not be null");
    }

    window.localStorage[storage] = cache;
  }

  @override
  Future<void> write(String value) async =>
      window.localStorage[storage] = value;

  @override
  Future<String> read() async => window.localStorage[storage];
}
