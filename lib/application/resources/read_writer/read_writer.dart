class ReadWriter {
  String cache;

  ReadWriter({this.cache = ''});

  // ignore: use_setters_to_change_properties
  Future<void> write(String value) async {
    cache = value;
  }

  Future<String> read() async => cache;
}
