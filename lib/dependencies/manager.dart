import 'dart:async';

import 'package:flutter/material.dart';

import 'package:mtg_counter/application/blocs/game.dart' as blocs;
import 'package:mtg_counter/application/blocs/games.dart' as blocs;

import 'model.dart';
import 'provider.dart';

class Manager extends StatefulWidget {
  final Widget child;
  final Model dependencies;
  final Widget splashScreen;

  const Manager({Key key, this.child, this.dependencies, this.splashScreen})
      : super(key: key);

  @override
  ManagerState createState() => ManagerState();
}

class ManagerState extends State<Manager> {
  StreamController<Object> updateDependenciesController;
  Stream<Object> updateDependencies;
  StreamSubscription<Object> updateDependenciesSubscription;

  StreamController<bool> isMustBeUpdateController;
  Stream<bool> isMustBeUpdate;

  @override
  void initState() {
    super.initState();

    isMustBeUpdateController = StreamController<bool>();
    isMustBeUpdate = isMustBeUpdateController.stream.asBroadcastStream();

    updateDependenciesController = StreamController<Object>();
    updateDependencies =
        updateDependenciesController.stream.asBroadcastStream();

    updateDependenciesSubscription = updateDependencies.listen((object) {
      switch (object.runtimeType) {
        case blocs.Games:
          widget.dependencies.games = object;
          break;

        case blocs.Game:
          widget.dependencies.game = object;
          break;
      }

      if (widget.dependencies.games != null &&
          widget.dependencies.game != null) {
        isMustBeUpdateController.add(true);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();

    isMustBeUpdateController.close();
    updateDependenciesSubscription.cancel();
    updateDependenciesController.close();
  }

  @override
  Widget build(BuildContext context) => StreamBuilder<bool>(
      stream: isMustBeUpdate,
      initialData:
          widget.dependencies.games == null && widget.dependencies.game == null
              ? false
              : true,
      builder: (context, snapshot) {
        if (snapshot.data) {
          return Provider(
              child: widget.child,
              updateDependenciesController: updateDependenciesController,
              dependencies: Model(
                  games: widget.dependencies.games,
                  game: widget.dependencies.game));
        } else {
          return Provider(
              updateDependenciesController: updateDependenciesController,
              dependencies: Model(
                  games: widget.dependencies.games,
                  game: widget.dependencies.game),
              child: MaterialApp(home: Scaffold(body: widget.splashScreen)));
        }
      });
}
