import 'package:mtg_counter/application/blocs/game.dart' as blocs;
import 'package:mtg_counter/application/blocs/games.dart' as blocs;

class Model {
  blocs.Games games;
  blocs.Game game;

  Model({this.games, this.game});
}
