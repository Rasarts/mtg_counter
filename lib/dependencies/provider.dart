import 'dart:async';

import 'package:flutter/widgets.dart';

import 'package:mtg_counter/application/blocs/game.dart' as blocs;
import 'package:mtg_counter/application/blocs/games.dart' as blocs;

import 'model.dart';

class Provider extends InheritedModel {
  final StreamController<Object> updateDependenciesController;

  final Model dependencies;

  const Provider(
      {this.dependencies, this.updateDependenciesController, Widget child})
      : super(child: child);

  @override
  bool updateShouldNotify(Provider oldWidget) {
    if (oldWidget.dependencies.games != dependencies.games) {
      return true;
    }

    if (oldWidget.dependencies.game.gameModel.dateTime !=
        dependencies.game.gameModel.dateTime) {
      return true;
    }

    return false;
  }

  @override
  bool updateShouldNotifyDependent(Provider oldWidget, Set dependencies) {
    if (dependencies.contains(blocs.Games) &&
        oldWidget.dependencies.games != this.dependencies.games) {
      return true;
    }

    if (dependencies.contains(blocs.Game) &&
        oldWidget.dependencies.game != this.dependencies.game) {
      return true;
    }

    return false;
  }
}
