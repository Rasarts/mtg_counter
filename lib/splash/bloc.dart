import 'dart:async';
import 'dart:convert';
import 'dart:io' show File;

import 'package:path_provider/path_provider.dart'
    show getApplicationDocumentsDirectory;

import 'package:mtg_counter/application/blocs/games.dart' as blocs;

import 'package:mtg_counter/application/resources/read_writer/file_read_writer.dart'
    as resources;
import 'package:mtg_counter/application/resources/read_writer/read_writer.dart'
    as resources;

import 'package:mtg_counter/application/models/games.dart' as models;

class Splash {
  Future<blocs.Games> prepareGamesBloc(
          StreamController<Object> injectorStreamForUpdateDependencies) async =>
      getApplicationDocumentsDirectory().then((directory) async {
        const fileName = 'games.json';
        final file = File('${directory.path}/$fileName');

        if (!file.existsSync()) file.createSync();
        final content = file.readAsStringSync();

        blocs.Games bloc;

        if (content.isEmpty) {
          bloc = blocs.Games(
              readWriter: resources.FileReadWriter(file: file),
              model: models.Games(),
              dependenciesController: injectorStreamForUpdateDependencies);

          await bloc.createGame();
        } else {
          final games = json.decode(content);
          final model = models.Games.fromList(games);

          bloc = blocs.Games(
              model: model,
              readWriter: resources.FileReadWriter(file: file),
              dependenciesController: injectorStreamForUpdateDependencies);
        }

        return bloc;
      });
}
