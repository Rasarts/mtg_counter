import 'package:flutter/rendering.dart';

class SplashScreenTheme {
  const SplashScreenTheme();

  static Color get backgroundColor => const Color(0xFFffffff);
  static Color get indicatorColor => const Color(0xFFe43866);
  static Color get indicatorTextColor => const Color(0xFFe43866);
}
