import 'dart:async';
import "dart:convert";

import "package:flutter/material.dart";

import "package:logging/logging.dart";

import 'package:mtg_counter/dependencies/provider.dart' as dependencies;

import 'package:mtg_counter/application/blocs/games.dart' as blocs;

import 'bloc.dart' as bloc show Splash;
import 'theme.dart';

/// Splash - loading screen.
class Screen extends StatefulWidget {
  @override
  ScreenState createState() => ScreenState();
}

class ScreenState extends State<Screen> {
  final Logger log = Logger("Splash");

  bloc.Splash splash = bloc.Splash();

  StreamController<String> messagesController;
  Stream<String> messages;

  dependencies.Provider provider;

  blocs.Games games;

  @override
  void initState() {
    super.initState();

    messagesController = StreamController<String>();
    messages = messagesController.stream.asBroadcastStream();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      provider = InheritedModel.inheritFrom<dependencies.Provider>(context);

      log.info(json.encode({"Message": "Prepare GamesBloc."}));
      messagesController.add('Prepare Games.');

      splash
          .prepareGamesBloc(provider.updateDependenciesController)
          .then((gamesBloc) {
        games = gamesBloc;

        log.info(json.encode({"Message": "Games done."}));

        provider.updateDependenciesController.add(gamesBloc);

        messagesController.add('Prepare Game.');
        log.info(json.encode({"Message": "Prepare Game."}));

        gamesBloc.selectGameById(gamesBloc.model.games.last.id);

        messagesController.add('Game done.');
        log.info(json.encode({"Message": "Game done."}));

        messagesController.add('Loading done.');
      });
    });

//    WidgetsBinding.instance.addPostFrameCallback((_) {
//      log.info(json.encode({"Message": "Prepare GameBloc."}));
//      messagesController.add('Prepare GameBloc.');
//
//      splashBloc.prepareGameBloc().then((gameBloc) {
//        log.info(json.encode({"Message": "GameBloc done."}));
//        messagesController.add('GameBloc done.');
//
//        InheritedModel.inheritFrom<InjectorModel>(context)
//            .updateDependenciesController
//            .add(gameBloc);
//
//        log.info(json.encode({"Message": "Prepare GamesBloc."}));
//        splashBloc.prepareGamesBloc().then((gamesBloc) {
//          log.info(json.encode({"Message": "GamesBloc done."}));
//
//          InheritedModel.inheritFrom<InjectorModel>(context)
//              .updateDependenciesController
//              .add(gamesBloc);
//
//          messagesController.add('Loading done.');
//        });
//      });
//    });
  }

  @override
  void dispose() {
    super.dispose();

    messagesController.close();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Center(
          child: Container(
            key: Key("splash_screen"),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            color: SplashScreenTheme.backgroundColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(
                  valueColor:
                      AlwaysStoppedAnimation(SplashScreenTheme.indicatorColor),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 10),
                  child: StreamBuilder<String>(
                      stream: messages,
                      initialData: 'Loading... \n\n',
                      builder: (context, snapshot) {
                        return Text(
                          snapshot.data,
                          style: TextStyle(
                              color: SplashScreenTheme.indicatorTextColor),
                        );
                      }),
                ),
              ],
            ),
          ),
        ),
      );
}
