@TestOn('vm')
library game_io_unit_test;

import 'dart:convert';
import 'dart:io';

import 'package:test/test.dart';

import 'package:mtg_counter/application/blocs/game.dart' as blocs;
import 'package:mtg_counter/application/blocs/player.dart' as blocs;

import 'package:mtg_counter/application/models/game.dart' as models;
import 'package:mtg_counter/application/models/player.dart' as models;

import 'package:mtg_counter/application/resources/read_writer/read_writer.dart'
    as resources;
import 'package:mtg_counter/application/resources/read_writer/file_read_writer.dart'
    as resources;

void main() {
  group('Game bloc', () {
    group('without file', () {
      models.Game gameModel;
      blocs.Game gameBloc;

      setUp(() {
        gameModel = models.Game();
        gameBloc = blocs.Game(
            gameModel: gameModel, readWriter: resources.ReadWriter());
      });

      tearDown(() {
        gameBloc.dispose();
      });

      group('Game', () {
        test('can load game without players', () async {
          gameBloc.load();

          await for (blocs.GameEvent gameEvent in gameBloc.game) {
            expect(gameEvent.gameEventType,
                equals(blocs.GameEventType.gameLoaded));
            expect(gameEvent.players, isEmpty);
            break;
          }
        });

        test('can load game with players', () async {
          gameBloc.addPlayer(models.Player(name: 'Text player name'));
          expect(gameBloc.players, isNotEmpty);

          await for (blocs.GameEvent gameEvent in gameBloc.game) {
            expect(gameEvent.gameEventType,
                equals(blocs.GameEventType.playerAdded));
            break;
          }

          gameBloc.load();

          await for (blocs.GameEvent gameEvent in gameBloc.game) {
            expect(gameEvent.gameEventType,
                equals(blocs.GameEventType.gameLoaded));
            expect(gameEvent.players, isNotEmpty);
            break;
          }
        });
      });

      group('Player', () {
        group('without events', () {
          test('can be added', () async {
            expect(gameModel.players, isEmpty);
            expect(gameBloc.players, isEmpty);

            gameBloc.addPlayer(models.Player(name: 'Test player name'));

            expect(gameModel.players, isNotEmpty);
            expect(gameBloc.players, isNotEmpty);
          });

          test('can be removed', () async {
            final playerModel = models.Player(name: 'Test player name');

            gameBloc.addPlayer(playerModel);
            expect(gameModel.players, isNotEmpty);
            expect(gameBloc.players, isNotEmpty);

            gameBloc.removePlayer(playerModel);

            expect(gameModel.players, isEmpty);
            expect(gameBloc.players, isEmpty);
          });
        });

        group('with events', () {
          test('can be added', () async {
            final playerModel = models.Player(name: 'Test player name');
            gameBloc.addPlayer(playerModel);

            await for (blocs.GameEvent gameEvent in gameBloc.game) {
              expect(gameEvent.gameEventType, blocs.GameEventType.playerAdded);
              expect(gameEvent.players, isNotEmpty);

              expect(gameEvent.changedPlayer.playerModel, equals(playerModel));

              expect(gameModel.players, isNotEmpty);
              expect(gameBloc.players, isNotEmpty);

              break;
            }
          });

          test('can be removed', () async {
            final playerModel = models.Player(name: 'Test player name');
            gameBloc.addPlayer(playerModel);
            expect(gameBloc.players, isNotEmpty);

            await for (blocs.GameEvent gameEvent in gameBloc.game) {
              if (gameEvent.gameEventType == blocs.GameEventType.playerAdded) {
                break;
              }
            }

            gameBloc.removePlayer(playerModel);
            expect(gameBloc.players, isEmpty);

            await for (blocs.GameEvent gameEvent in gameBloc.game) {
              expect(
                  gameEvent.gameEventType, blocs.GameEventType.playerRemoved);
              expect(gameEvent.players, isEmpty);

              expect(gameEvent.changedPlayer.playerModel, equals(playerModel));

              expect(gameModel.players, isEmpty);
              expect(gameBloc.players, isEmpty);

              break;
            }
          });
        });
      });
    });

    group('with file', () {
      File file;
      resources.ReadWriter readWriter;
      models.Game gameModel;
      blocs.Game gameBloc;

      setUp(() {
        file = File('./game_read_writer_cache.json')..createSync();
        readWriter = resources.FileReadWriter(file: file);
        gameModel = models.Game();
        gameBloc = blocs.Game(gameModel: gameModel, readWriter: readWriter);
      });

      tearDown(() {
        file.deleteSync();
        gameBloc.dispose();
      });

      test('can be rewrited by change player event', () async {
        final playerModel = models.Player(name: 'Test player name');
        gameBloc.addPlayer(playerModel);
        final playerBloc = gameBloc.players.first;

        expect(gameBloc.gameModel.players.first.name, equals(playerModel.name));

        const updatedPlayerName = 'Update test player name';
        playerBloc.changeName(updatedPlayerName);

        await for (blocs.PlayerEvent event in playerBloc.playerEvents) {
          expect(
              event.playerEventType, equals(blocs.PlayerEventType.nameChange));
          break;
        }

        expect(
            gameBloc.gameModel.players.first.name, equals(updatedPlayerName));

        final Map<String, dynamic> cache = json.decode(readWriter.cache);
        final updatedGameModel = models.Game.fromMap(cache);

        expect(updatedGameModel.players.first.name, equals(updatedPlayerName));
      });

      group('Player', () {
        test('can be added', () async {
          expect(gameModel.players, isEmpty);
          expect(gameBloc.players, isEmpty);

          gameBloc.addPlayer(models.Player(name: 'Test player name'));

          expect(gameModel.players, isNotEmpty);
          expect(gameBloc.players, isNotEmpty);

          final decodedGame = json.decode(await readWriter.read());
          expect(decodedGame, equals(gameModel.toMap()));
        });

        test('can be removed', () async {
          final playerModel = models.Player(name: 'Test player name');

          gameBloc.addPlayer(playerModel);
          expect(gameModel.players, isNotEmpty);
          expect(gameBloc.players, isNotEmpty);

          gameBloc.removePlayer(playerModel);

          expect(gameModel.players, isEmpty);
          expect(gameBloc.players, isEmpty);

          final decodedGame = json.decode(await readWriter.read());
          expect(decodedGame, equals(gameModel.toMap()));
        });
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
