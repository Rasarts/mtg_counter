@TestOn('vm')
library player_io_unit_test;

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:test/test.dart';

import 'package:mtg_counter/application/blocs/games.dart' as blocs;
import 'package:mtg_counter/application/blocs/game.dart' as blocs;

import 'package:mtg_counter/application/models/game.dart' as models;
import 'package:mtg_counter/application/models/game_details.dart' as models;

import 'package:mtg_counter/application/models/games.dart' as models;
import 'package:mtg_counter/application/models/player.dart' as models;

import 'package:mtg_counter/application/resources/read_writer/file_read_writer.dart'
    as resources;
import 'package:mtg_counter/application/resources/read_writer/read_writer.dart'
    as resources;

void main() {
  group('Games bloc', () {
    models.Games gamesModel;
    blocs.Games gamesBloc;

    const gamesFilePath = 'games.json';

    setUp(() {
      final file = File(gamesFilePath)..createSync();
      final readWriter = resources.FileReadWriter(file: file);
      gamesModel = models.Games();
      gamesBloc = blocs.Games(model: gamesModel, readWriter: readWriter);
    });

    tearDown(() => File(gamesFilePath).deleteSync());

    group('Game', () {
      test('can be created', () async {
        expect(gamesModel.details.length, equals(0));
        expect(gamesModel.games.length, equals(0));

        final firstGame = await gamesBloc.createGame();

        expect(gamesModel.details.length, equals(1));
        expect(gamesModel.details.first.selected, isTrue);

        expect(gamesModel.games.length, equals(1));

        final models.Game secondGame = await gamesBloc.createGame();

        expect(gamesModel.games.length, equals(2));

        expect(secondGame, isNotNull);
        expect(secondGame.id, isNotNull);
        expect(secondGame.dateTime, isNotNull);
        expect(secondGame.players, isEmpty);
        expect(secondGame.startLifeCount, equals(20));

        expect(gamesModel.details.length, equals(2));
        expect(gamesModel.details.first.selected, isFalse);
        expect(gamesModel.details.last.selected, isTrue);
        expect(gamesModel.details.last.id, equals(secondGame.id));
        expect(
            gamesModel.details.last.filePath, equals('${secondGame.id}.json'));

        final List gamesJSON =
            json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(2));
        expect(gamesJSON.first['selected'], isFalse);
        expect(gamesJSON.last['selected'], isTrue);

        final Map firstGameJSON =
            json.decode(File('${firstGame.id}.json').readAsStringSync());
        expect(firstGameJSON.isNotEmpty, isTrue);

        final Map secondGameJSON =
            json.decode(File('${secondGame.id}.json').readAsStringSync());
        expect(secondGameJSON.isNotEmpty, isTrue);

        await gamesBloc.deleteGame(firstGame);
        await gamesBloc.deleteGame(secondGame);
      });

      test('can be created by reset game model (play again)', () async {
        final firstGame = await gamesBloc.createGame();

        expect(gamesModel.details.length, equals(1));
        expect(gamesModel.details.first.selected, isTrue);

        final firstPlayer = models.Player(name: 'First test player');
        final secondPlayer =
            models.Player(name: 'Second test player', lifeCount: 1);

        expect(gamesModel.games.length, equals(1));

        final existGame = models.Game(
            startLifeCount: 40, players: [firstPlayer, secondPlayer]);

        final models.Game secondGame =
            await gamesBloc.createGameFromAlreadyExist(existGame);

        expect(gamesModel.games.length, equals(2));

        expect(secondGame, isNotNull);
        expect(secondGame.id, isNotNull);
        expect(secondGame.dateTime, isNotNull);

        expect(secondGame.players, isNotEmpty);
        expect(secondGame.players.length, equals(2));
        expect(secondGame.players.last.id, equals(secondPlayer.id));
        expect(secondGame.players.last.lifeCount, equals(40));

        expect(secondGame.startLifeCount, equals(40));

        expect(gamesModel.details.length, equals(2));
        expect(gamesModel.details.first.selected, isFalse);
        expect(gamesModel.details.last.selected, isTrue);
        expect(gamesModel.details.last.id, equals(secondGame.id));
        expect(
            gamesModel.details.last.filePath, equals('${secondGame.id}.json'));

        final List gamesJSON =
            json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(2));

        final Map firstGameJSON =
            json.decode(File('${firstGame.id}.json').readAsStringSync());
        expect(firstGameJSON.isNotEmpty, isTrue);

        final Map secondGameJSON =
            json.decode(File('${secondGame.id}.json').readAsStringSync());
        expect(secondGameJSON.isNotEmpty, isTrue);

        await gamesBloc.deleteGame(firstGame);
        await gamesBloc.deleteGame(secondGame);
      });

      test('can be selected', () async {
        final firstGame = await gamesBloc.createGame();

        expect(gamesModel.details.length, equals(1));
        expect(gamesModel.details.first.selected, isTrue);

        final secondGame = await gamesBloc.createGame();

        expect(gamesModel.details.length, equals(2));
        expect(gamesModel.details.first.selected, isFalse);
        expect(gamesModel.details.last.selected, isTrue);

        await gamesBloc.selectGameById(gamesModel.details.first.id);

        expect(gamesModel.details.first.selected, isTrue);
        expect(gamesModel.details.last.selected, isFalse);

        final List gamesJSON =
            json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(2));

        final Map firstGameJSON =
            json.decode(File('${firstGame.id}.json').readAsStringSync());
        expect(firstGameJSON.isNotEmpty, isTrue);

        final Map secondGameJSON =
            json.decode(File('${secondGame.id}.json').readAsStringSync());
        expect(secondGameJSON.isNotEmpty, isTrue);

        await gamesBloc.deleteGame(firstGame);
        await gamesBloc.deleteGame(secondGame);
      });

      test('can be deleted', () async {
        final models.Game firstGame = await gamesBloc.createGame();

        expect(gamesModel.games.length, equals(1));
        expect(gamesModel.details.first.selected, isTrue);

        List gamesJSON = json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(1));

        final Map firstGameJSON =
            json.decode(File('${firstGame.id}.json').readAsStringSync());
        expect(firstGameJSON.isNotEmpty, isTrue);

        await gamesBloc.deleteGame(firstGame);

        expect(gamesModel.games.length, equals(0));

        gamesJSON = json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(0));

        final file = File('${firstGame.id}.json');
        expect(file.existsSync(), isFalse);
      });
    });

    group('Game with dependencies', () {
      StreamController<Object> dependenciesController;
      Stream<Object> dependenciesObjects;

      const gamesFilePath = 'games.json';

      setUp(() {
        dependenciesController = StreamController<Object>();
        dependenciesObjects = dependenciesController.stream.asBroadcastStream();

        final file = File(gamesFilePath)..createSync();
        final readWriter = resources.FileReadWriter(file: file);
        gamesModel = models.Games();

        gamesBloc = blocs.Games(
            model: gamesModel,
            readWriter: readWriter,
            dependenciesController: dependenciesController);
      });

      tearDown(() async {
        await dependenciesController.close();
      });

      test('can be created', () async {
        expect(gamesModel.details.length, equals(0));
        expect(gamesModel.games.length, equals(0));

        final game = await gamesBloc.createGame();

        await for (blocs.Game game in dependenciesObjects) {
          expect(game.gameModel.players, isEmpty);
          expect(game.gameModel.startLifeCount, equals(20));

          expect(gamesModel.details.first.selected, isTrue);

          break;
        }

        expect(gamesModel.games.length, equals(1));
        expect(gamesModel.details.length, equals(1));
        expect(gamesModel.details.first.selected, isTrue);

        await gamesBloc.deleteGame(game);
      });

      test('can be created by reset game model (play againe)', () async {
        expect(gamesModel.details, isEmpty);
        expect(gamesModel.games, isEmpty);

        final firstPlayer = models.Player(name: 'First test player');
        final secondPlayer =
            models.Player(name: 'Second test player', lifeCount: 1);

        final existGame = models.Game(
            startLifeCount: 40, players: [firstPlayer, secondPlayer]);

        final models.Game game =
            await gamesBloc.createGameFromAlreadyExist(existGame);

        await for (blocs.Game game in dependenciesObjects) {
          expect(
              game.gameModel.players.first.name, equals('First test player'));
          expect(
              game.players.first.playerModel.name, equals('First test player'));

          expect(
              game.gameModel.players.last.name, equals('Second test player'));
          expect(
              game.players.last.playerModel.name, equals('Second test player'));

          expect(game.gameModel.players.last.lifeCount, equals(40));
          expect(game.players.last.playerModel.lifeCount, equals(40));

          break;
        }

        expect(gamesModel.details.length, equals(1));
        expect(gamesModel.games.length, equals(1));

        expect(game, isNotNull);
        expect(game.id, isNotNull);
        expect(game.dateTime, isNotNull);

        expect(game.players, isNotEmpty);
        expect(game.players.length, equals(2));
        expect(game.players.last.id, equals(secondPlayer.id));
        expect(game.players.last.lifeCount, equals(40));

        expect(game.startLifeCount, equals(40));

        expect(gamesModel.details.length, equals(1));

        expect(gamesModel.details.last.selected, isTrue);

        expect(gamesModel.details.last.id, equals(game.id));
        expect(gamesModel.details.last.filePath, equals('${game.id}.json'));

        await gamesBloc.deleteGame(game);
      });

      test('can be selected', () async {
        expect(gamesModel.details, isEmpty);

        final firstGame = await gamesBloc.createGame();

        expect(gamesModel.details.length, equals(1));
        expect(gamesModel.details.last.selected, isTrue);

        await gamesBloc.selectGameById(gamesModel.details.first.id);

        await for (blocs.Game game in dependenciesObjects) {
          expect(game.gameModel.id, equals(firstGame.id));
          break;
        }

        expect(gamesModel.details.last.selected, isTrue);

        final secondGame = await gamesBloc.createGame();

        expect(gamesModel.details.length, equals(2));

        expect(gamesModel.details.first.selected, isFalse);
        expect(gamesModel.details.last.selected, isTrue);

        await gamesBloc.selectGameById(firstGame.id);

        await for (blocs.Game game in dependenciesObjects) {
          expect(game.gameModel.id, equals(firstGame.id));
          break;
        }

        expect(gamesModel.details.first.selected, isTrue);
        expect(gamesModel.details.last.selected, isFalse);

        await gamesBloc.deleteGame(firstGame);
        await gamesBloc.deleteGame(secondGame);
      });

      test('can be deleted', () async {
        final models.Game firstGame = await gamesBloc.createGame();
        final models.Game secondGame = await gamesBloc.createGame();

        expect(File('${firstGame.id}.json').existsSync(), isTrue);
        expect(File('${secondGame.id}.json').existsSync(), isTrue);

        expect(gamesModel.games.length, equals(2));
        expect(gamesModel.details.first.selected, isFalse);
        expect(gamesModel.details.last.selected, isTrue);

        List gamesJSON = json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(2));

        dependenciesController = StreamController<Object>();
        dependenciesObjects = dependenciesController.stream.asBroadcastStream();
        gamesBloc.dependenciesController = dependenciesController;

        await gamesBloc.selectGameById(firstGame.id);

        await for (blocs.Game game in dependenciesObjects) {
          expect(game.gameModel.id, equals(firstGame.id));
          break;
        }

        expect(gamesModel.details.first.selected, isTrue);
        expect(gamesModel.details.last.selected, isFalse);

        dependenciesController = StreamController<Object>();
        dependenciesObjects = dependenciesController.stream.asBroadcastStream();
        gamesBloc.dependenciesController = dependenciesController;

        await gamesBloc.deleteGame(firstGame);

        await for (blocs.Game game in dependenciesObjects) {
          expect(game.gameModel.id, equals(secondGame.id));
          break;
        }

        expect(gamesModel.games.length, equals(1));

        gamesJSON = json.decode(File(gamesFilePath).readAsStringSync());
        expect(gamesJSON.length, equals(1));

        expect(File('${firstGame.id}.json').existsSync(), isFalse);
        expect(File('${secondGame.id}.json').existsSync(), isTrue);

        await gamesBloc.deleteGame(secondGame);
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
