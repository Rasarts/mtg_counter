import 'package:flutter_test/flutter_test.dart';

import 'package:mtg_counter/application/blocs/player.dart' as blocs;
import 'package:mtg_counter/application/models/player.dart' as models;

void main() {
  group('Player bloc', () {
    group('without events', () {
      models.Player playerModel;
      blocs.Player playerBloc;

      setUp(() {
        playerModel = models.Player(name: 'Test player name');
        playerBloc = blocs.Player(playerModel);
      });

      tearDown(() => playerBloc.dispose());

      test('can change name', () async {
        const newName = 'Updated test player name';

        final oldName = playerModel.name;
        expect(oldName, isNot(newName));

        playerBloc.changeName(newName);

        final updatedName = playerModel.name;
        expect(oldName, isNot(updatedName));
        expect(updatedName, equals(newName));
      });

      test('can add color', () async {
        expect(playerModel.colors, isEmpty);

        playerBloc.addColor(models.PlayerColor.black);

        expect(playerModel.colors, isNotEmpty);
        expect(playerModel.colors.first, equals(models.PlayerColor.black));
      });

      test('can remove color', () async {
        playerModel.colors.add(models.PlayerColor.white);
        expect(playerModel.colors, isNotEmpty);
        expect(playerModel.colors.first, equals(models.PlayerColor.white));

        playerBloc.removeColor(models.PlayerColor.white);

        expect(playerModel.colors, isEmpty);
      });

      test('can add life quantity', () async {
        expect(playerModel.lifeCount, equals(20));
        playerBloc.addLifeQuantity(5);
        expect(playerModel.lifeCount, equals(25));
        playerBloc.addLifeQuantity(-5);
        expect(playerModel.lifeCount, equals(20));
      });

      test('can remove life quantity', () async {
        expect(playerModel.lifeCount, equals(20));
        playerBloc.removeLifeQuantity(5);
        expect(playerModel.lifeCount, equals(15));
        playerBloc.removeLifeQuantity(-5);
        expect(playerModel.lifeCount, equals(10));
      });

      test('can add poison quantity', () async {
        expect(playerModel.poisonCount, equals(0));
        playerBloc.addPoisonQuantity(5);
        expect(playerModel.poisonCount, equals(5));
        playerBloc.addPoisonQuantity(-5);
        expect(playerModel.poisonCount, equals(0));
      });

      test('can remove poison quantity', () async {
        expect(playerModel.poisonCount, equals(0));
        playerBloc.removePoisonQuantity(5);
        expect(playerModel.poisonCount, equals(-5));
        playerBloc.removePoisonQuantity(-5);
        expect(playerModel.poisonCount, equals(-10));
      });
    });

    group('with events', () {
      models.Player playerModel;
      blocs.Player playerBloc;

      setUp(() {
        playerModel = models.Player(name: 'Test player name');
        playerBloc = blocs.Player(playerModel);
      });

      test('can change name', () async {
        const newName = 'Updated test player name';

        final oldName = playerModel.name;
        expect(oldName, isNot(newName));

        playerBloc.changeName(newName);

        await for (String updatedName in playerBloc.name) {
          expect(oldName, isNot(updatedName));
          expect(updatedName, equals(newName));
          expect(updatedName, equals(playerModel.name));
          break;
        }
      });

      test('can add color', () async {
        expect(playerModel.colors, isEmpty);

        playerBloc.addColor(models.PlayerColor.black);

        await for (List<models.PlayerColor> colors in playerBloc.colors) {
          expect(colors, isNotEmpty);
          expect(colors.first, equals(models.PlayerColor.black));
          break;
        }
      });

      test('can remove color', () async {
        playerModel.colors.add(models.PlayerColor.white);
        expect(playerModel.colors, isNotEmpty);
        expect(playerModel.colors.first, equals(models.PlayerColor.white));

        playerBloc.removeColor(models.PlayerColor.white);

        await for (List<models.PlayerColor> colors in playerBloc.colors) {
          expect(colors, isEmpty);
          break;
        }
      });

      test('can add life quantity', () async {
        expect(playerModel.lifeCount, equals(20));
        playerBloc.addLifeQuantity(5);

        await for (int lifeCount in playerBloc.lifeCount) {
          expect(lifeCount, equals(25));
          break;
        }

        playerBloc.addLifeQuantity(-5);

        await for (int lifeCount in playerBloc.lifeCount) {
          expect(lifeCount, equals(20));
          break;
        }
      });

      test('can remove life quantity', () async {
        expect(playerModel.lifeCount, equals(20));
        playerBloc.removeLifeQuantity(5);

        await for (int lifeCount in playerBloc.lifeCount) {
          expect(lifeCount, equals(15));
          break;
        }

        playerBloc.removeLifeQuantity(-5);

        await for (int lifeCount in playerBloc.lifeCount) {
          expect(lifeCount, equals(10));
          break;
        }
      });

      test('can add poison quantity', () async {
        expect(playerModel.poisonCount, equals(0));
        playerBloc.addPoisonQuantity(5);

        await for (int poisonCount in playerBloc.poisonCount) {
          expect(poisonCount, equals(5));
          break;
        }

        playerBloc.addPoisonQuantity(-5);

        await for (int poisonCount in playerBloc.poisonCount) {
          expect(poisonCount, equals(0));
          break;
        }
      });

      test('can remove poison quantity', () async {
        expect(playerModel.poisonCount, equals(0));
        playerBloc.removePoisonQuantity(5);

        await for (int poisonCount in playerBloc.poisonCount) {
          expect(poisonCount, equals(-5));
          break;
        }

        playerBloc.removePoisonQuantity(-5);

        await for (int poisonCount in playerBloc.poisonCount) {
          expect(poisonCount, equals(-10));
          break;
        }
      });
    });
  });
}
