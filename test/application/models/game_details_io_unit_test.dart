@TestOn('vm')
library game_details_io_unit_test;

import 'package:test/test.dart';

import 'package:mtg_counter/application/models/game_details.dart';

void main() {
  group('GameDetails', () {
    GameDetails gameDetails;

    setUp(() {
      gameDetails = GameDetails(id: '#', filePath: './');
    });

    group('map', () {
      test('can be encoded', () {
        final encodedObject = gameDetails.toMap();

        final expectedObject = {
          'id': '#',
          'dateTime': null,
          'filePath': './',
          'selected': false
        };

        expect(encodedObject, equals(expectedObject));
      });

      test('can be decoded', () {
        final encodedObject = {
          'id': '#',
        };

        final decodedObject = GameDetails.fromMap(encodedObject);

        expect(decodedObject.id, equals(gameDetails.id));
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
