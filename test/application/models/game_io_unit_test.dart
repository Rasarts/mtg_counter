@TestOn('vm')
library game_io_unit_test;

import 'package:test/test.dart';

import 'package:mtg_counter/application/models/game.dart';
import 'package:mtg_counter/application/models/player.dart';

void main() {
  group('Game model', () {
    List<Player> players;
    Game gameModel;

    setUp(() {
      players = [Player(name: 'Test player name')];
      gameModel = Game(players: players, startLifeCount: 30);
    });

    group('map', () {
      test('can be encoded', () {
        final encodedObject = gameModel.toMap();

        final expectedObject = {
          'id': gameModel.id,
          'dateTime': gameModel.dateTime.toIso8601String(),
          'startLifeCount': 30,
          'players': [
            {
              'id': players.first.id,
              'name': 'Test player name',
              'colors': [],
              'lifeCount': 20,
              'poisonCount': 0
            }
          ]
        };

        expect(encodedObject, equals(expectedObject));
      });

      test('can be decoded', () {
        final encodedObject = {
          'id': gameModel.id,
          'dateTime': gameModel.dateTime.toIso8601String(),
          'startLifeCount': 30,
          'players': [
            {
              'id': players.first.id,
              'name': 'Test player name',
              'colors': [],
              'lifeCount': 20,
              'poisonCount': 0
            }
          ]
        };

        final decodedObject = Game.fromMap(encodedObject);

        expect(decodedObject.id, equals(gameModel.id));
        expect(decodedObject.dateTime, equals(gameModel.dateTime));
        expect(
            decodedObject.players.first.id, equals(gameModel.players.first.id));
        expect(decodedObject.players.first.name,
            equals(gameModel.players.first.name));
        expect(decodedObject.players.first.colors,
            equals(gameModel.players.first.colors));
        expect(decodedObject.players.first.lifeCount,
            equals(gameModel.players.first.lifeCount));
        expect(decodedObject.players.first.poisonCount,
            equals(gameModel.players.first.poisonCount));
        expect(decodedObject.startLifeCount, equals(30));
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
