import 'package:test/test.dart';

import 'package:mtg_counter/application/models/game_details.dart';
import 'package:mtg_counter/application/models/games.dart';

void main() {
  group('GamesModel', () {
    Games gamesModel;

    setUp(() {
      gamesModel = Games(details: [GameDetails(id: "0"), GameDetails(id: "1")]);
    });

    group('map', () {
      test('can be encoded', () {
        final encodedObject = gamesModel.toList();

        final expectedObject = [
          {'id': '0', 'dateTime': null, 'filePath': null, 'selected': false},
          {'id': '1', 'dateTime': null, 'filePath': null, 'selected': false}
        ];

        expect(encodedObject, equals(expectedObject));
      });

      test('can be decoded', () {
        final encodedObject = [
          {'id': '0'},
          {'id': '1'}
        ];

        final decodedObject = Games.fromList(encodedObject);

        expect(decodedObject.details.length, equals(encodedObject.length));
        expect(
            decodedObject.details.first.id, equals(encodedObject.first['id']));
        expect(decodedObject.details.last.id, equals(encodedObject.last['id']));
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
