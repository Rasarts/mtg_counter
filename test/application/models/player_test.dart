@TestOn('vm')
library player_io_unit_test;

import 'package:test/test.dart';

import 'package:mtg_counter/application/models/player.dart';

void main() {
  group('Player model', () {
    const String name = 'Test player name';
    final List<PlayerColor> colors = [PlayerColor.black, PlayerColor.white];

    Player playerModel;

    setUp(() {
      playerModel = Player(name: name, colors: colors);
    });

    group('map', () {
      test('can be encoded', () {
        final encodedObject = playerModel.toMap();

        final expectedObject = {
          'id': playerModel.id,
          'name': name,
          'colors': ['black', 'white'],
          'lifeCount': 20,
          'poisonCount': 0
        };

        expect(encodedObject, equals(expectedObject));
      });

      test('can be decoded', () {
        final encodedObject = {
          'id': playerModel.id,
          'name': name,
          'colors': ['black', 'white'],
          'lifeCount': 20,
          'poisonCount': 0
        };

        final decodedObject = Player.fromMap(encodedObject);

        expect(decodedObject.id, equals(playerModel.id));
        expect(decodedObject.name, equals(name));
        expect(decodedObject.colors, equals(colors));
        expect(decodedObject.lifeCount, equals(20));
        expect(decodedObject.poisonCount, equals(0));
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
