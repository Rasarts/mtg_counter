@TestOn('vm')
library file_read_writer_unit_test;

import 'dart:io';

import 'package:test/test.dart';

import 'package:mtg_counter/application/resources/read_writer/read_writer.dart';
import 'package:mtg_counter/application/resources/read_writer/file_read_writer.dart';

void main() {
  group('ReadWriter', () {
    group('with file', () {
      File file;
      ReadWriter readWriter;

      setUp(() {
        file = File('./read_writer_cache.json')..createSync();
        readWriter = FileReadWriter(file: file);
      });

      tearDown(() {
        file.deleteSync();
      });

      test('can write', () async {
        const testValue = 'test value';
        await readWriter.write(testValue);
        expect(file.readAsStringSync(), equals(testValue));
      });

      test('can read', () async {
        final value = await readWriter.read();
        expect(value, isEmpty);

        const testValue = 'test value';
        await readWriter.write(testValue);

        final updatedValue = await readWriter.read();
        expect(updatedValue, equals(testValue));
      });
    });

    group('without file', () {
      ReadWriter readWriter;
      setUp(() {
        readWriter = ReadWriter();
      });

      test('can write', () async {
        const testValue = 'test value';
        await readWriter.write(testValue);
        expect(readWriter.cache, equals(testValue));
      });

      test('can read', () async {
        final value = await readWriter.read();
        expect(value, isEmpty);

        const testValue = 'value';
        readWriter.cache = testValue;

        final updatedValue = await readWriter.read();
        expect(updatedValue, equals(testValue));
      });
    });
  }, onPlatform: {'vm': Timeout.factor(2)}, tags: ['io', 'unit']);
}
