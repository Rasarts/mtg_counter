@TestOn('browser')
library localstorage_read_writer_browser_unit_test;

import 'dart:html';

import 'package:mtg_counter/application/resources/read_writer/local_storage_read_writer.dart';
import 'package:test/test.dart';

import 'package:mtg_counter/application/resources/read_writer/read_writer.dart';

void main() {
  group('ReadWriter', () {
    group('with localstorage', () {
      String path;
      ReadWriter readWriter;

      setUp(() {
        path = 'read_writer_cache.json';
        readWriter = LocalStorageReadWriter(storage: path);
      });

      tearDown(() {
        window.localStorage.remove(path);
      });

      test('can write', () async {
        const testValue = 'test value';
        await readWriter.write(testValue);
        expect(await readWriter.read(), equals(testValue));
      });

      test('can read', () async {
        final value = await readWriter.read();
        expect(value, isEmpty);

        const testValue = 'test value';
        await readWriter.write(testValue);

        final updatedValue = await readWriter.read();
        expect(updatedValue, equals(testValue));
      });
    });

    group('without file', () {
      ReadWriter readWriter;
      setUp(() {
        readWriter = ReadWriter();
      });

      test('can write', () async {
        const testValue = 'test value';
        await readWriter.write(testValue);
        expect(readWriter.cache, equals(testValue));
      });

      test('can read', () async {
        final value = await readWriter.read();
        expect(value, isEmpty);

        const testValue = 'value';
        readWriter.cache = testValue;

        final updatedValue = await readWriter.read();
        expect(updatedValue, equals(testValue));
      });
    });
  }, onPlatform: {'browser': Timeout.factor(2)}, tags: ['browser', 'unit']);
}
