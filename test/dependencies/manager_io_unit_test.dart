@TestOn('vm')
library manager_io_unit_test;

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mtg_counter/application/blocs/game.dart' as blocs;
import 'package:mtg_counter/application/blocs/games.dart' as blocs;

import 'package:mtg_counter/application/models/game.dart' as models;
import 'package:mtg_counter/application/models/games.dart' as models;

import 'package:mtg_counter/application/resources/read_writer/read_writer.dart'
    as resources;

import 'package:mtg_counter/dependencies/manager.dart' as dependencies;
import 'package:mtg_counter/dependencies/provider.dart' as dependencies;
import 'package:mtg_counter/dependencies/model.dart' as dependencies;

void main() {
  group('Dependencies manager', () {
    testWidgets('can change splash screen to application screen',
        (tester) async {
      final splash = SplashScreen();
      final application = ApplicationScreen();

      final model = dependencies.Model();
      final manager = dependencies.Manager(
        splashScreen: splash,
        child: application,
        dependencies: model,
      );

      final widget = MaterialApp(home: Scaffold(body: manager));

      await tester.runAsync(() => tester.pumpWidget(widget));

      expect(find.byKey(Key('application_screen')), findsNothing);
      expect(find.byKey(Key('splash_screen')), findsOneWidget);

      final _SplashScreenState state = tester.state(find.byWidget(splash));

      final game = blocs.Game(
          gameModel: models.Game(), readWriter: resources.ReadWriter());

      state.dependenciesController.add(game);
      await tester.pump();

      expect(find.byKey(Key('application_screen')), findsNothing);
      expect(find.byKey(Key('splash_screen')), findsOneWidget);

      final games = blocs.Games(
          model: models.Games(), readWriter: resources.ReadWriter());

      state.dependenciesController.add(games);

      await tester.runAsync(() => Future.delayed(Duration(seconds: 1)));
      await tester.pump();

      expect(find.byKey(Key('application_screen')), findsOneWidget);
      expect(find.byKey(Key('splash_screen')), findsNothing);
    });
  });
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // ignore: close_sinks
  StreamController<Object> dependenciesController;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) {
      dependenciesController =
          InheritedModel.inheritFrom<dependencies.Provider>(context)
              .updateDependenciesController;
    });

//    Future.delayed(
//        Duration(seconds: 1),
//        () => InheritedModel.inheritFrom<dependencies.Provider>(context)
//            .updateDependenciesController
//            .add(blocs.Game()));
//
//    Future.delayed(
//        Duration(seconds: 2),
//        () => InheritedModel.inheritFrom<dependencies.Provider>(context)
//            .updateDependenciesController
//            .add(blocs.Games()));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: Key('splash_screen'),
    );
  }
}

class ApplicationScreen extends StatefulWidget {
  @override
  _ApplicationScreenState createState() => _ApplicationScreenState();
}

class _ApplicationScreenState extends State<ApplicationScreen> {
  blocs.Game game;
  blocs.Games games;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    game = InheritedModel.inheritFrom<dependencies.Provider>(context)
        .dependencies
        .game;
    games = InheritedModel.inheritFrom<dependencies.Provider>(context)
        .dependencies
        .games;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: Key('application_screen'),
    );
  }
}
