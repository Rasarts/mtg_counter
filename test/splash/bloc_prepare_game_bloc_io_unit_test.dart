@TestOn("vm")
library bloc_prepare_game_bloc_io_unit_test;

import 'dart:async';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:mtg_counter/application/blocs/games.dart' as blocs;

import 'package:mtg_counter/splash/bloc.dart' as bloc;

void main() {
  group("Splash bloc", () {
    StreamController updateDependenciesController;
    bloc.Splash splash;

    setUpAll(() async {
      final directory = await Directory.systemTemp.createTemp();

      const MethodChannel('plugins.flutter.io/path_provider')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getApplicationDocumentsDirectory') {
          return directory.path;
        } else {
          return null;
        }
      });
    });

    setUp(() {
      updateDependenciesController = StreamController();
      splash = bloc.Splash();
    });

    testWidgets('can prepare dependencies', (tester) async {
      final blocs.Games games =
          await splash.prepareGamesBloc(updateDependenciesController);

      final firstGameId = games.model.games.first.id;
      expect(firstGameId, isNotNull);
      expect(firstGameId, isNotEmpty);

      final firstGameFilePath = games.model.details.first.filePath;

      expect(firstGameFilePath, isNotNull);
      expect(firstGameFilePath, isNotEmpty);

      final firstGameFile = File(firstGameFilePath);
      expect(firstGameFile.existsSync(), isTrue);
      firstGameFile.deleteSync();
    });
  });
}
