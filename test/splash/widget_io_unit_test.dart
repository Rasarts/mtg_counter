@TestOn('vm')
library widget_io_unit_test;

import 'dart:io';

import 'package:flutter_test/flutter_test.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:mtg_counter/application/blocs/game.dart' as blocs;
import 'package:mtg_counter/application/blocs/games.dart' as blocs;
import 'package:mtg_counter/application/blocs/player.dart' as blocs;

import 'package:mtg_counter/dependencies/manager.dart' as dependencies;
import 'package:mtg_counter/dependencies/provider.dart' as dependencies;
import 'package:mtg_counter/dependencies/model.dart' as dependencies;

import 'package:mtg_counter/splash/widget.dart' as splash;

void main() {
  group('Splash', () {
    Directory directory;

    setUpAll(() async {
      directory = await Directory.systemTemp.createTemp();

      const MethodChannel('plugins.flutter.io/path_provider')
          .setMockMethodCallHandler((MethodCall methodCall) async {
        if (methodCall.method == 'getApplicationDocumentsDirectory') {
          return directory.path;
        } else {
          return null;
        }
      });
    });

    testWidgets('can change splash screen to application screen',
        (tester) async {
      final testedWidget = splash.Screen();
      final application = ApplicationScreen();

      final model = dependencies.Model();
      final manager = dependencies.Manager(
        splashScreen: testedWidget,
        child: application,
        dependencies: model,
      );

      final widget = MaterialApp(home: Scaffold(body: manager));

      await tester.pumpWidget(widget);

      expect(find.byKey(Key('application_screen')), findsNothing);
      expect(find.byKey(Key('splash_screen')), findsOneWidget);

      final splash.ScreenState testedWidgetState =
          tester.state(find.byWidget(testedWidget));

      await tester.pump();

      expect(find.byKey(Key('application_screen')), findsOneWidget);
      expect(find.byKey(Key('splash_screen')), findsNothing);

      final gameFile =
          File(testedWidgetState.games.model.details.first.filePath);

      // ignore: cascade_invocations
      gameFile.deleteSync();
    });
  });
}

class ApplicationScreen extends StatefulWidget {
  @override
  _ApplicationScreenState createState() => _ApplicationScreenState();
}

class _ApplicationScreenState extends State<ApplicationScreen> {
  blocs.Game game;
  blocs.Games games;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    game = InheritedModel.inheritFrom<dependencies.Provider>(context)
        .dependencies
        .game;
    games = InheritedModel.inheritFrom<dependencies.Provider>(context)
        .dependencies
        .games;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: Key('application_screen'),
    );
  }
}
