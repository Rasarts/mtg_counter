import 'package:flutter_driver/driver_extension.dart';

import 'application_test.dart' as application;

void main() {
  enableFlutterDriverExtension();
  application.main();
}
